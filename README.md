# intro-coding-for-journalists

This repo is for class assignments and resources for UT-Austin School of Journalism course Intro to Coding for Journalists.

The Canvas course is your official courseware, but some assignments and files may be in this repository.